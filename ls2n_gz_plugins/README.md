[![License](https://img.shields.io/badge/License-Apache%202.0-blue.svg)](https://opensource.org/licenses/Apache-2.0)

ls2n_gz_plugins
=====================
Gazebo plugins that are specific for the ls2n_drone_ros2 environment.

fpr_init
--------
This plugin creates an assembly with several drones and a poly-articulated structure in Gazebo. This can be used as a
reference to assemble drones with other elements outside the PX4 simulation environment.

mass_and_cables_init
--------
This plugin creates an assembly with several drones and a mass with "cables" (represented by two spherical and one 
prismatic joints)