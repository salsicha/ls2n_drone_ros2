import rclpy
from threading import Thread
from rclpy.node import Node
from rclpy.qos import qos_profile_sensor_data
from std_msgs.msg import UInt64, Float32
from ls2n_interfaces.msg import *
from PyQt5.QtWidgets import *
import pyqtgraph as pg


qos_profile_sensor_data.depth = 1


class ScopeData:
    def __init__(self, max_duration):
        self.max_duration = max_duration
        self.time = []
        self.data = []
        self.refresh_graph = False
        self.counter_refresh = 0

    def add_data(self, time, data):
        self.time.append(time)
        self.data.append(data)
        while self.time[-1] - self.time[0] > self.max_duration:
            self.time.pop(0)
            self.data.pop(0)
        self.counter_refresh += 1
        if self.counter_refresh > 50:
            self.refresh_graph = True

    def must_refresh(self):
        if self.refresh_graph:
            self.counter_refresh = 0
            self.refresh_graph = False
            return True
        return False


class Supervisor(Node):
    def __init__(self, gui_interface):
        super().__init__('drone_supervision')
        self.time_init = self.get_clock().now().nanoseconds/1e9
        self.get_logger().info("Starting drone supervision")
        self.create_subscription(UInt64,
                                 "RTPSBridgeDelay",
                                 self.rtps_delay_callback,
                                 qos_profile=qos_profile_sensor_data,
                                 )
        # To check the wifi delay we base our measure on the callback message from keepAlive
        self.wifi_delay_counter = 0
        self.wifi_delay = 0
        self.create_subscription(KeepAlive,
                                 "KeepAlive",
                                 self.wifi_delay_callback,
                                 qos_profile=qos_profile_sensor_data,
                                 )
        self.gui_interface = gui_interface
        # Obesrver data subscription
        self.create_subscription(Float32,
                                 "Observer/MaxThrustObserved",
                                 self.max_thrust_callback,
                                 qos_profile=qos_profile_sensor_data
                                 )
        self.create_subscription(Float32,
                                 "ObserverMonitor/MaxThrustObserved",
                                 self.max_thrust_callback,
                                 qos_profile=qos_profile_sensor_data
                                 )
        self.create_subscription(Disturbances,
                                 "Observer/DisturbancesWorld",
                                 self.disturbances_callback,
                                 qos_profile=qos_profile_sensor_data
                                 )
        self.create_subscription(Disturbances,
                                 "ObserverMonitor/DisturbancesWorld",
                                 self.disturbances_callback,
                                 qos_profile=qos_profile_sensor_data
                                 )
        self.max_thrust_data = ScopeData(10.0)
        self.disturbances_x_data = ScopeData(10.0)
        self.disturbances_y_data = ScopeData(10.0)
        self.disturbances_z_data = ScopeData(10.0)

    def rtps_delay_callback(self, msg):
        delay = int(msg.data / 1000)  # Delay in ms
        if self.gui_interface['tabs']:
            self.gui_interface['rtps_delay_bar'].setValue(delay)

    def wifi_delay_callback(self, msg):
        if msg.feedback:
            time_now = self.get_clock().now().to_msg()
            # Delay in ms
            delay = ((time_now.sec - msg.origin_stamp.sec)*1000 + (time_now.nanosec - msg.origin_stamp.nanosec)*1e-6)/2.0
            self.wifi_delay += delay/10.0
            self.wifi_delay_counter += 1
            if self.wifi_delay_counter >= 10:
                self.wifi_delay = int(self.wifi_delay)
                if self.gui_interface['tabs'].isVisible():
                    self.gui_interface['wifi_delay_bar'].setValue(self.wifi_delay)
                self.wifi_delay_counter = 0
                self.wifi_delay = 0

    def max_thrust_callback(self, msg):
        time_data = self.get_clock().now().nanoseconds/1e9 - self.time_init
        self.max_thrust_data.add_data(time_data, msg.data)
        if self.gui_interface['tabs'] and self.max_thrust_data.must_refresh():
            curve = self.gui_interface["max_thrust_curve"]
            curve.setData(self.max_thrust_data.time, self.max_thrust_data.data)

    def disturbances_callback(self, msg):
        time_data = self.get_clock().now().nanoseconds/1e9 - self.time_init
        self.disturbances_x_data.add_data(time_data, msg.x)
        self.disturbances_y_data.add_data(time_data, msg.y)
        self.disturbances_z_data.add_data(time_data, msg.z)
        if self.gui_interface['tabs'] and self.disturbances_x_data.must_refresh() \
                and self.disturbances_y_data.must_refresh():
            curve = self.gui_interface["disturbances_curve_x"]
            curve.setData(self.disturbances_x_data.time, self.disturbances_x_data.data)
            curve = self.gui_interface["disturbances_curve_y"]
            curve.setData(self.disturbances_y_data.time, self.disturbances_y_data.data)
            curve = self.gui_interface["disturbances_curve_z"]
            curve.setData(self.disturbances_z_data.time, self.disturbances_z_data.data)


def main(args=None):
    rclpy.init(args=args)
    # Creating the gui
    gui = QApplication([])
    tabs = QTabWidget()
    # Delay tab
    delay_tab = QWidget()
    rtps_delay_text = QLabel("RTPS delay")
    rtps_delay_bar = QProgressBar()
    rtps_delay_bar.setFormat("%v ms")
    rtps_delay_bar.setMinimum(0)
    rtps_delay_bar.setMaximum(100)
    rtps_delay_bar.setValue(0)
    wifi_delay_text = QLabel("WIFI delay")
    wifi_delay_bar = QProgressBar()
    wifi_delay_bar.setFormat("%v ms")
    wifi_delay_bar.setMinimum(0)
    wifi_delay_bar.setMaximum(100)
    wifi_delay_bar.setValue(0)
    delay_grid = QGridLayout()
    delay_grid.addWidget(rtps_delay_text, 0, 0)
    delay_grid.addWidget(rtps_delay_bar, 0, 1)
    delay_grid.addWidget(wifi_delay_text, 1, 0)
    delay_grid.addWidget(wifi_delay_bar, 1, 1)
    delay_tab.setLayout(delay_grid)
    tabs.addTab(delay_tab, "Delays")
    # Observer tab
    observer_tab = QWidget()
    observer_layout = QVBoxLayout()
    plot_max_thrust = pg.PlotWidget()
    plot_disturbance = pg.PlotWidget()
    observer_layout.addWidget(QLabel("Max Thrust"))
    observer_layout.addWidget(plot_max_thrust)
    observer_layout.addWidget(QLabel("Disturbances"))
    observer_layout.addWidget(plot_disturbance)
    observer_tab.setLayout(observer_layout)
    tabs.addTab(observer_tab, "Observer")
    tabs.show()

    # Creating the gui interface for the node
    gui_interface = {
        "gui_main": gui,
        "tabs": tabs,
        "rtps_delay_bar": rtps_delay_bar,
        "wifi_delay_bar": wifi_delay_bar,
        "max_thrust_curve": plot_max_thrust.getPlotItem().plot(pen=pg.mkPen('g', width=2)),
        "disturbances_curve_x": plot_disturbance.getPlotItem().plot(pen=pg.mkPen('g', width=2)),
        "disturbances_curve_y": plot_disturbance.getPlotItem().plot(pen=pg.mkPen('r', width=2)),
        "disturbances_curve_z": plot_disturbance.getPlotItem().plot(pen=pg.mkPen('w', width=2)),
    }

    # Creating the node
    node = Supervisor(gui_interface)
    node_thread = Thread(target=rclpy.spin, args=(node,))
    node_thread.daemon = True
    node_thread.start()
    gui.exec()
    node.destroy_node()
    print("Shutting down drone supervision")


if __name__ == 'main':
    main()
