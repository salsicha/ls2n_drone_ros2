from launch import LaunchDescription
from launch_ros.actions import Node
import os
import yaml
from ament_index_python.packages import get_package_share_directory
from datetime import datetime

now = datetime.now()
dt_string = now.strftime("%d-%m-%Y_%H:%M:%S")

config_file = os.path.join(
    get_package_share_directory('ls2n_drone_bridge'),
    'config',
    'px4_params.yaml'
)
with open(config_file) as file:
    config = yaml.load(file, Loader=yaml.FullLoader)

drone_name = list(config.keys())[0]

offboard_nodes = [Node(package='ls2n_drone_bridge',
                       executable='drone_bridge',
                       output='screen',
                       namespace=drone_name,
                       parameters=[config_file])]

if config[drone_name]["drone_bridge"]["ros__parameters"]["observer"]:
    offboard_nodes.append(Node(package='ls2n_drone_bridge',
                               executable='observer_node',
                               output='screen',
                               namespace=drone_name,
                               parameters=[config_file]))


def generate_launch_description():
    launch_commands = [Node(package='ls2n_px4_ros_com',
                            executable='micrortps_agent',
                            output='screen',
                            namespace=drone_name,
                            parameters=[
                                {"transport": "UART"},
                                {"device": "/dev/ttyS0"},
                                {"baudrate": 921600}
                            ])
                       ] + offboard_nodes
    return LaunchDescription(launch_commands)
