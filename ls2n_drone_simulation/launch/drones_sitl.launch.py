from launch import LaunchDescription
from launch.actions import DeclareLaunchArgument
from launch.actions import ExecuteProcess
from launch.actions import OpaqueFunction
from ament_index_python.packages import get_package_share_directory
from launch_ros.actions import Node
from launch.substitutions import LaunchConfiguration
import os
import yaml
from datetime import datetime

now = datetime.now()
dt_string = now.strftime("%d-%m-%Y_%H:%M:%S")


# Set the bridge parameters according to the drone type for simulation
def get_bridge_parameters(drone_type):
    mass = 1.0
    max_thrust = 47.0
    kp = 3.0
    kd = 1.5
    ki = 0.0
    if drone_type == 'crazyflie2':
        mass = 0.032
        max_thrust = 0.56
        kp = 3.0
        kd = 2.0
        ki = 0.1
    return {'mass': mass, 'max_thrust': max_thrust, 'kp': kp, 'kd': kd, 'ki': ki}


def launch_setup(context, *args, **kwargs):
    px4_folder_file = os.path.join(
        get_package_share_directory('ls2n_drone_simulation'),
        'config',
        'simu_folders.yaml'
    )
    with open(px4_folder_file) as file:
        folders_config = yaml.load(file, Loader=yaml.FullLoader)

    model = LaunchConfiguration("drone_model").perform(context) + "_rtps"
    px4_folder = folders_config['ls2n_drone_simulation']['launch_parameters']['px4_folder']
    try:
        n_drones = int(LaunchConfiguration("number_of_drones").perform(context))
    except ValueError:
        print("Number of drones should be an integer.")
        exit(os.EX_USAGE)
    gz_model_path = os.path.join(get_package_share_directory('ls2n_drone_simulation'), 'models')
    gz_plugin_path = os.path.join(get_package_share_directory('ls2n_gz_plugins'), 'gz_plugins')

    # One bridge node per drone is started
    start_bridge_nodes = [Node(
        package='ls2n_drone_bridge', executable='drone_bridge',
        output='screen', namespace='Drone' + str(i + 1),
        parameters=[get_bridge_parameters(LaunchConfiguration("drone_model").perform(context))])
        for i in range(n_drones)]

    # One observer node per drone is started
    start_observer_nodes = []
    if LaunchConfiguration("observer_node").perform(context) == "true":
        start_observer_nodes = [Node(
            package='ls2n_drone_bridge', executable='observer_node',
            output='screen', namespace='Drone' + str(i + 1),
            parameters=[{'max_thrust_broadcast': True, 'disturbance_xy_broadcast': True, 'gain': 2.0}])
            for i in range(n_drones)]

    return [
               # PX4 script to spawn the drones (firmware sitl + gazebo)
               ExecuteProcess(
                   cmd=[px4_folder + '/Tools/gazebo_sitl_multiple_run.sh', '-m',
                        model, '-n', str(n_drones), '-t', 'px4_sitl_rtps',
                        '-w', LaunchConfiguration('gz_world').perform(context),
                        '-a', gz_plugin_path,
                        '-b', gz_model_path]
               )
           ] + start_bridge_nodes + start_observer_nodes


def generate_launch_description():
    return LaunchDescription([
        DeclareLaunchArgument('drone_model', default_value='crazy2fly',
                              description='Drone model', choices=['crazy2fly', 'crazyflie2']),
        DeclareLaunchArgument('number_of_drones', default_value='1',
                              description='Number of drones'),
        DeclareLaunchArgument('observer_node', default_value='false',
                              description='Activate the observer node', choices=['true', 'false']),
        DeclareLaunchArgument('gz_world', default_value='empty',
                              description='Gazebo world to use'),
        OpaqueFunction(function=launch_setup)
    ])
