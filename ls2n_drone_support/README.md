[![License](https://img.shields.io/badge/License-Apache%202.0-blue.svg)](https://opensource.org/licenses/Apache-2.0)

ls2n_drone_support
=====================
ls2n_drone_support is a ROS2 package aimed providing tools for drones maintenance.