[![License](https://img.shields.io/badge/License-Apache%202.0-blue.svg)](https://opensource.org/licenses/Apache-2.0)

LS2N_drone_simulation
=====================
LS2N_drone_simulation is a ROS2 package aimed at helping the user to launch a simulation using gazebo and a SITL
simulation of the PX4.

Prerequisites
--------------------------

1. In the PX4-Autopilot folder, build in SITL mode and with rtps support and gazebo plugins
    ```console
    DONT_RUN=1 make px4_sitl_rtps gazebo
    ```
2. Set proper folder path to PX4-Autopilot in ls2_drones_simulation/config/px4_folder.yaml

Usage
---------------------------
Use this command line to run the simulation

   ```console
      ros2 launch ls2n_drone_simulation drones_sitl.launch.py
   ```

It will run Gazebo, the PX4 SITL simulation, the microRTPS bridge and the drone_bridge nodes. Several drones can be
launched at the same time. In this case, you have to pass the proper launch arguments to drone_sitl.launch.py. Once the
simulation is run, you can use the drone_bridge topics to start the motors and interact with the drone (see the specific
documentation for this package).

Supported drones for now are:

- crazy2fly
- crazyflie2

You can run a working simulation of a hovering drone using

   ```console
      ros2 launch ls2n_drone_simulation single_drone_trajectory_sitl.launch.py
   ```

Also, you can run specific multi drones devices, for example:

- The flying parallel robot using
   ```console
      ros2 launch ls2n_drone_simulation fpr_sitl.launch.py
   ```