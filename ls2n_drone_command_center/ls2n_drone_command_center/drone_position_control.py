import rclpy
from rclpy.node import Node
from rclpy.qos import qos_profile_sensor_data
from std_srvs.srv import Empty
from ls2n_interfaces.srv import *
from rclpy.callback_groups import ReentrantCallbackGroup
qos_profile_sensor_data.depth = 1


class DronePositionControl(Node):
    def __init__(self):
        super().__init__('direct_position_control')
        self.cb_group = ReentrantCallbackGroup()
        self.create_service(
            Empty,
            "SpinMotors",
            self.spin_motors,
            callback_group=self.cb_group
        )
        self.create_service(
            Empty,
            "StartExperiment",
            self.start_experiment,
            callback_group=self.cb_group
        )
        self.create_service(
            Empty,
            "StopExperiment",
            self.stop_experiment,
            callback_group=self.cb_group
        )
        self.drone_request_client = self.create_client(
            DroneRequest,
            "Request",
            callback_group=self.cb_group
        )
        self.start_trajectory_client = self.create_client(
            Empty,
            "StartTrajectory"
        )
        self.reset_trajectory_client = self.create_client(
            Empty,
            "ResetTrajectory"
        )
        self.experiment_started = False

    async def spin_motors(self, request, response):
        if not self.experiment_started:
            request_out = DroneRequest.Request()
            request_out.request = DroneRequest.Request.SPIN_MOTORS
            future = self.drone_request_client.call_async(request_out)
            await future
            if future.result().success:
                self.get_logger().info("Drone motors spinning")
            else:
                self.get_logger().info("Failed arming drone")
        else:
            self.get_logger().info("Experiment already started. Stop experiment to reset.")
        return response

    async def start_experiment(self, request, response):
        if not self.experiment_started:
            request_out = DroneRequest.Request()
            request_out.request = DroneRequest.Request.TAKE_OFF
            future = self.drone_request_client.call_async(request_out)
            await future
            if future.result().success:
                self.get_logger().info("Drone is flying")
                request_out = DroneRequest.Request()
                request_out.request = DroneRequest.Request.POSITION_CONTROL
                future = self.drone_request_client.call_async(request_out)
                await future
                if future.result().success:
                    future = self.start_trajectory_client.call_async(Empty.Request())
                    self.experiment_started = True
                else:
                    self.get_logger().info("Failed to switch to position control. Experiment aborted.")
            else:
                self.get_logger().info("Failed to take off. Experiment aborted.")
        else:
            self.get_logger().info("Experiment already started.")
        return response

    def stop_experiment(self, request, response):
        self.get_logger().info("Stopping drone")
        request_out = DroneRequest.Request()
        request_out.request = DroneRequest.Request.LAND
        self.drone_request_client.call_async(request_out)
        self.reset_trajectory_client.call_async(Empty.Request())
        self.experiment_started = False
        return response


def main(args=None):
    rclpy.init(args=args)
    node = DronePositionControl()
    try:
        rclpy.spin(node)
    except KeyboardInterrupt:
        print('Shutting down drone position control')
    finally:
        node.destroy_node()
        rclpy.shutdown()


if __name__ == 'main':
    main()
