[![License](https://img.shields.io/badge/License-Apache%202.0-blue.svg)](https://opensource.org/licenses/Apache-2.0)

ls2n_drone_bridge
=====================
LS2N_drone_bridge is a ROS2 package to interact with the drones available at the LS2N laboratory. It is designed to
minimise the data exchange between the autopilot and the onboard computer and keep low response time (compared to
MAVLINK). However, all functionalities are not available.

Supported firmwares
--------------------------
PX4 with DDS protocol (micro_rtps agent)

Usage
---------------------------
This bridge can be used either:

- In simulation: Use the *ls2n_drones_simulation* package to launch your simulation environment.
- Offboard: Use the offboard_bridge.launch.py. Bridge setup must be done in config/px4_params.yaml in this case.

The classic usage of this bridge is to use the *SpinMotors* service to start the motors and arm the drone. Then, start
the appropriate control mode using *StartControl* service. To use a given control, the appropriate input topic should be
used (i.e., *Trajectory* for position control, *AttitdeThrustSetPoint* for attitude_thrust, ...).

Topics
--------------------------
Topics ending with "PubSubTopic" are automatically generated from the rtps agent and are complicated to use as they are
directly translated as a UORB topic for the PX4 controller. It is thus recommended to not use those topics unless you
know exactly what you are doing. You may use following topics and services to interact with the bridge.

- *KeepAlive*: This topic ensures that communication is up with the bridge. Offboard mode is maintained only if the keep
  alive is up. Is no messages are received on this topic for more than 0.1 second, the drone goes in "emergency
  shutdown" mode.
- *Status*: Provides feedback about the drone status.
- *Trajectory*: Publish to this topic when controlling the drone with the position or velocity controller.
- *AttitudeThrustSetPoint*: Set points for attitude/thrust control. Attitude in quaternion and thrust from 0 to
  max_thrust.
- *MotorControlSetPoint*: Set points for direct motor control, scaled from 0 to 1.
- *Mocap/odom*: MOCAP informations to be sent to the drone.
- *EKF/odom*: Odometry computed by the drone (with internal EKF).

Services
----------------------------

- *Request*: To send a DroneRequest to the bridge (see service definition for available requests)
- *GetParameters*: The response to this service is the drones parmeters (mass, max_thrust)
